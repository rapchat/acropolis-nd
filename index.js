
/*
module.exports = {
  delos: require('./hellas/delos'),
  delphi: require('./hellas/delphi'),
  athena: require('./hellas/athena'),
  rodos: require('./hellas/rodos'),
};
*/

import * as delos from './hellas/delos.js';
import * as delphi from './hellas/delphi.js';
import * as athena from './hellas/athena.js';
import * as rodos from './hellas/rodos.js';

export { delos, delphi, athena, rodos };

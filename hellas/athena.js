

const percent = (part, whole) => (100 * (part / whole)) - 100;
const dtAddMinutes = (dt, minutes) => new Date(dt.getTime() + (minutes * 60000));
const dtDiffMs = (dtStart, dtEnd) => dtEnd.getTime() - dtStart.getTime();
const isNumber = (value) => {
  if (typeof value !== 'number' || value !== Number(value) || Number.isFinite(value) === false) { return false; }
  return true;
};

const ngrams = async (aString, minSize = 3, prefixOnly = false) => {
  const rt = [];
  const ngramPrefix = (txt) => {
    for (let i = minSize; i <= Math.max(txt.length, minSize); i += 1) { rt.push(txt.slice(0, i)); }
  };
  if (prefixOnly === true) {
    ngramPrefix(aString);
  } else {
    for (let j = 0; j <= aString.length - minSize; j += 1) { ngramPrefix(aString.slice(j)); }
  }
  return rt;
};

const convertMS = (ms, asString = true) => {
  const pad = (v, n = 2) => v.toString().padStart(n, '0');
  let s = Math.floor(ms / 1000);
  let m = Math.floor(s / 60);
  s %= 60;
  let h = Math.floor(m / 60);
  m %= 60;
  const d = Math.floor(h / 24);
  h %= 24;
  if (asString === true) { return `${pad(d)}:${pad(h)}:${pad(m)}:${pad(s)}`; }
  return { d, h, m, s };
};

const randomBetween = (min, max) => Math.floor(Math.random() * (max - min + 1) + min); // random integer between min - nax (included)
const dtToStrCompressed = dt => dt.toISOString().slice(2, 19).replace(/[-:T]/g, ''); // returned format YYmmDDhhMMss
const dtTObjUtc = (dt) => {
  return { y: dt.getUTCFullYear(), m: dt.getUTCMonth(), d: dt.getUTCDate(), h: dt.getUTCHours(), M: dt.getUTCMinutes() };
};

class DateRandom {
  constructor(dtStart = new Date(0), dtEnd = new Date()) {   // default start on epoch start
    this.dtStart = dtStart;
    this.dtEnd = dtEnd;
    this.tsjStart = dtStart.getTime(); // js time start = epoch ts * 1000
    this.tsjEnd = dtEnd.getTime();
  }

  get randomEpoch() { return this.randomTs / 1000; }

  get randomTs() { return randomBetween(this.tsjStart, this.tsjEnd) }

  get randomDt() { return new Date(this.randomTs);}
}

export {
  ngrams,
  isNumber,
  percent,
  dtAddMinutes,
  dtDiffMs,
  convertMS,
  dtToStrCompressed,
  DateRandom,
  dtTObjUtc,
};

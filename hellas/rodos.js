/**
 * a Logger that supports common logger functions but does nothing
 * useful to switch between real logging in debug mode and  logging without match overhead
 * @warning do not inherit;
 * @usage DummyLogger.log(1); (don't instantiate it)
 */
class DummyLogger {
  static log() {}

  static debug() {}

  static info() {}

  static warn() {}

  static error() {}

  static time() {}

  static timeEnd() {}
}

class LevelCounter {
  constructor(startCounter = 1) {
    this.store = {};
    this.startCounter = startCounter;
  }

  inc(level, val = 1) {
    this.store[level] = (this.store[level] === undefined) ? this.startCounter : this.store[level] += val;
    return this.store[level];
  }

  toStr(level, separator = '.', padTo = 0, padChr = '0') {
    let values = Object.values(this.store).splice(0, level + 1);
    if (padTo !== 0) { values = values.map(x => x.toString().padStart(padTo, padChr)) };
    return values.splice(0, level + 1).join(separator);
  }
}

export { DummyLogger, LevelCounter };

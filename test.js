
/* eslint-disable no-console */

// import { LevelCounter } from './hellas/rodos.js';

import { rodos } from './index.js';

const { LevelCounter } = rodos;

const testImports = () => {
  const lv = new LevelCounter();
  lv.inc(0);
  console.log('lv str', lv.toStr(1));
};

testImports();

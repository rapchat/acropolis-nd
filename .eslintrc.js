module.exports = {
  env: {
    es6: true,
    node: true,
  },
  extends: [
    'airbnb-base',
  ],
  globals: {
    Atomics: 'readonly',
    SharedArrayBuffer: 'readonly',
  },
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
  },
  rules: {
    "max-len": [1, 160, 2, {ignoreComments: true}],
    "arrow-body-style": 0,
    "prefer-spread": ["error"],
    "object-curly-newline": ["error", {"consistent": true}],
    "object-property-newline": ["error"],
    "import/extensions": ["error", "never", "ignorePackages"],
    "import/prefer-default-export": 'off',
  },
};
